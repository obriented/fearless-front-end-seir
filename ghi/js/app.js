function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card shadow p-3 mb-3 bg-white rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${starts} - ${ends}
            </div>
        </div>
    `;
}

function placeHolder(cardIndex) {
    return `
        <div id="card${cardIndex}">
            <div class="card shadow p-3 mb-3 bg-white rounded" aria-hidden="true">
                <img src="https://via.placeholder.com/150?text=" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title placeholder-glow">
                        <span class="placeholder col-6"></span>
                    </h5>
                    <p class="card-text placeholder-glow">
                        <span class="placeholder col-7"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-4"></span>
                        <span class="placeholder col-6"></span>
                        <span class="placeholder col-8"></span>
                    </p>
                    <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
                </div>
            </div>
        </div>
    `;
}

function alert() {
    return `
    <div class="alert alert-primary d-flex align-items-center" role="alert">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
        </svg>
        <div>
            Unable to load conferences
        </div>
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);
        if (!response.ok) {
            const container = document.querySelector('.container');
            container.innerHTML = alert();
        } else {
            const data = await response.json();
            let cardIndex = 0;
            for (let i = 0; i < data.conferences.length; i++) {
                if (cardIndex % 3 === 0) {
                    const column = document.querySelector('#col1');
                    column.innerHTML += placeHolder(cardIndex);
                } else if (cardIndex % 3 === 1) {
                    const column = document.querySelector('#col2');
                    column.innerHTML += placeHolder(cardIndex);
                } else {
                    const column = document.querySelector('#col3');
                    column.innerHTML += placeHolder(cardIndex);
                };
                cardIndex++;
            }
            cardIndex = 0;

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (cardIndex === 0) {

                }
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const startDate = new Date(details.conference.starts);
                    const endDate = new Date(details.conference.ends);
                    const starts = startDate.toDateString();
                    const ends = endDate.toDateString();
                    const location = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const card = document.querySelector(`#card${cardIndex}`)
                    card.innerHTML = html;
                }
                cardIndex ++;
            }
        }
    } catch (e) {
        console.log(e);
    }

});
